git clone https://github.com/explosion/spaCy.git
cd spaCy
git checkout tags/v2.2.3 -b wikidata_nel
cd bin/wiki_entity_linking
python wikidata_pretrain_kb.py '../../../../Wikipedia/WikiData/latest-all.json.bz2' '../../../../Wikipedia/enwiki-20200401-pages-articles-multistream.xml.bz2' '../../../pressdb/newsKG/vox/models/WikiKB' 'en_core_web_lg'
