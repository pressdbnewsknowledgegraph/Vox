# Import Packages
## Installed Packages
import spacy
import networkx as nx
import pytest

## Custom Packages
import src.information_extraction as ie


nlp = spacy.load('en_core_web_sm')

# Example from SpaCy documentation: https://spacy.io/usage/linguistic-features#dependency-parse
test_sentence = "Autonomous cars shift insurance liability toward manufacturers"
test_DT = nx.Graph()
test_DT.add_edges_from([(1, 0), (2, 1), (2, 4), (2, 5), (4, 3), (5, 6)])

# Example from "Dependency Parsing" by Sandra Kübler, Ryan McDonald, and Joakim Nivre
test_sentence_2 = "Economic news had little effect on financial markets"
test_DT_2 = nx.Graph()
test_DT_2.add_edges_from([(1, 0), (2, 1), (2, 4), (4, 3), (4, 5), (5, 7), (7, 6)])

# Example from "Speech and Language Processing" by Daniel Jurafsky and James H. Martin
test_sentence_3 = "I gave him my address"
test_DT_3 = nx.Graph()
test_DT_3.add_edges_from([(1, 0), (1, 2), (1, 4), (4, 3)])

# Example inspired by https://www.cs.utexas.edu/~ml/papers/spk-emnlp-05.pdf
test_sentence_4 = "Students bought several expensive textbooks"
test_DT_4 = nx.Graph()
test_DT_4.add_edges_from([(1, 0), (1, 4), (4, 2), (4, 3)])


test_dependency_tree_params = [
    (test_sentence, test_DT),
    (test_sentence_2, test_DT_2),
    (test_sentence_3, test_DT_3),
    (test_sentence_4, test_DT_4)
]


@pytest.mark.parametrize(
    'test_data, expected_output',
    test_dependency_tree_params
)
def test_dependency_tree(test_data, expected_output):
    assert nx.is_isomorphic(expected_output, ie.get_dependency_tree(test_data, nlp))


test_sdp = ["cars", "shift", "liability"]

test_sdp_2 = ["news", "had", "effect"]

test_sdp_3 = ["I", "gave", "address"]

test_sdp_4 = ["Students", "bought", "textbooks"]

test_sdp_params = [
    ((test_DT, 1, 4), test_sdp),
    ((test_DT_2, 1, 4), test_sdp_2),
    ((test_DT_3, 0, 4), test_sdp_3),
    ((test_DT_4, 0, 4), test_sdp_4)
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_sdp_params
)
def test_sdp(test_data, expected_output):
    assert ie.get_shortest_dependency_path(test_data[0], test_data[1], test_data[2]) == expected_output


test_relation_params = [
    (test_sdp, "SHIFT"),
    (test_sdp_2, "HAD"),
    (test_sdp_3, "GAVE"),
    (test_sdp_4, "BOUGHT")
]


@pytest.mark.parametrize(
    'test_data, expected_output',
    test_relation_params
)
def test_extract_relation(test_data, expected_output):
    assert ie.extract_relation(test_data) == expected_output
