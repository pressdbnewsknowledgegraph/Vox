# Import Packages
## Standard Packages

## Installed Packages
import spacy
import neuralcoref # Compatibility bug: https://github.com/huggingface/neuralcoref/issues/197
import networkx as nx
import pandas as pd
import spotlight
from envparse import env

# Custom Functions
from src.information_extraction import get_dependency_tree, get_shortest_dependency_path, extract_relation
from src.neo4j_etl import update_database


def string_cleaning(string):
    return string.lower().strip().replace('"', "'")


assert string_cleaning("Here's why I'm still optimistic.") == "here's why i'm still optimistic."

assert string_cleaning("Here's why \"I'm\" still optimistic.") == "here's why 'i'm' still optimistic."

assert string_cleaning("Here's why \"I'm\" still optimistic.  ") == "here's why 'i'm' still optimistic."

# Neo4j Credentials
env.read_envfile('.env')
uri = env.str('NEO4J_URL')
user = env.str('NEO4J_USR')
pwd = env.str('NEO4J_PWD')


# Basic pipeline for Information Extraction on Vox article headlines:
# - Data cleaning
# - Named entity recognition + noun phrases (excluding common nouns) via SpaCy
# - Coreference resolution via SpaCy
# - Relation extraction via shortest dependency path between two entities
# - Entity linking for all entities derived from Named Entity Recognition (except noun phrases), done via DBpedia Spotlight
# - Entity resolution: 
#     a. merge any entities determined to be the same via entity linking
#     b. String similarity join between entity names
# - Relation resolution: 
#     a. merge any relations that are semantically similar (word embeddings?)
#     b. String similarity join between relation names?




# NER and Coreference Resolution Pipeline
nlp = spacy.load('en_core_web_lg')
pipeline = ["merge_entities", "merge_noun_chunks"]
for name in pipeline:
    component = nlp.create_pipe(name)
    nlp.add_pipe(component)
neuralcoref.add_to_pipe(nlp)

# Load Dataset
# NOTE: Dataset created using ../src/data/download_dataset.py and ../src/data/clean_dataset.py
df = pd.read_csv('data/interim/dsjVoxArticlesCleaned.tsv', sep='\t', encoding='utf-8')

df['title_enr_lower'] = df['title_enr'].apply(string_cleaning)


# TODO: Entity Linking via DBpedia Spotlight (setup own server)
# docker pull dbpedia/spotlight-english
# docker run -itd --restart unless-stopped --name spotlight_en -p 2222:80 dbpedia/spotlight-english spotlight.sh

# Container has been updated here: https://hub.docker.com/r/dbpedia/dbpedia-spotlight

df = df.head()
# Only use handful of records to lighten load on DBpedia Spotlight Demo API


# Graph Schema
## Nodes: Articles (Properties: Title, Category, Published Date, Updated On), Entities, Authors
## Edges: EntityLinkedToArticle, ArticleWrittenByAuthor
G = nx.DiGraph()
## Add Articles and Authors to Knowledge Graph
for index, row in df.iterrows():
    G.add_node(row['title_enr_lower'], title=row['title_enr_lower'], category=row['category'],
               published_date=row['published_date'],
               updated_date=row['updated_on'], url=row['slug'],
               node_type='Article')
    G.add_node(row['author'], author=row['author'], node_type='Author')
    G.add_edge(row['title_enr_lower'], row['author'], relation="WRITTEN_BY")


# NER + Coreference Resolution on Article Titles
for index, row in df.iterrows():
    data = row['title_enr']
    doc = nlp(data)
    data_coref = doc._.coref_resolved
    doc = nlp(data_coref, disable=["neuralcoref"])
    ## Add entities from NER to Knowledge Graph
    for ent in doc.ents:
        entity_enr = string_cleaning(ent.text)
        if entity_enr not in G.nodes():
            G.add_node(entity_enr, entity=entity_enr, node_type='Entity',
                       entity_type=ent.label_)
        G.add_edge(entity_enr, row['title_enr_lower'], relation="ENTITY_IN_ARTICLE")


# Add Entities from Entity Linking to Knowledge Graph
for index, row in df.iterrows():
    try:
        annotate_tmp = spotlight.annotate('https://api.dbpedia-spotlight.org/en/annotate',
                                          row['title_enr'],
                                          confidence=0.5)
        for annotate in annotate_tmp:
            entity_enr = string_cleaning(annotate["surfaceForm"])

            # If entity from entity linking is not already in knowledge graph
            if entity_enr not in G.nodes():
                G.add_node(entity_enr, entity=entity_enr, node_type='Entity',
                           entity_link=annotate["URI"])
                G.add_edge(entity_enr, row['title_enr_lower'], relation="ENTITY_IN_ARTICLE")

            # If entity is already in knowledge graph, but does not have URI
            else:
                if "entity_link" not in G.nodes()[entity_enr].keys():
                    G.nodes()[entity_enr]["entity_link"] = annotate["URI"]
    except spotlight.SpotlightException:
        continue


# Binary Relation Extraction on Article Titles
for index, row in df.iterrows():
    data = row['title_enr']
    doc = nlp(data)
    data_coref = doc._.coref_resolved
    doc = nlp(data_coref, disable=["neuralcoref"])
    for sent in doc.sents:
        # For each entity/noun chunk in each sentence, get the index of the first token of that entity/noun chunk
        dependency_tree = get_dependency_tree(sentence=sent.string, nlp=nlp)

        for token1 in sent:
            for token2 in sent:
                if token1.i < token2.i and token1.pos_ == "NOUN" and token2.pos_ == "NOUN":
                    sdp = get_shortest_dependency_path(dependency_tree=dependency_tree,
                                                       entity1=token1.i, entity2=token2.i)
                    relation = extract_relation(sdp=sdp)
                    entity1 = string_cleaning(token1.text)
                    entity2 = string_cleaning(token2.text)

                    # If entity1 and entity2 are already in KG, add edge between them
                    if entity1 in G.nodes() and entity2 in G.nodes():
                        G.add_edge(entity1, entity2, relation=relation, from_article=row['title_enr_lower'])

                    # If entity1 is in KG, but entity2 is not, add node for entity2, then add edge between them
                    elif entity1 in G.nodes() and entity2 not in G.nodes():
                        G.add_node(entity2, entity=entity2, node_type='Entity')
                        G.add_edge(entity1, entity2, relation=relation, from_article=row['title_enr_lower'])
                        G.add_edge(entity2, row['title_enr_lower'], relation="ENTITY_IN_ARTICLE")

                    # If entity1 is not in KG, but entity2 is, add node for entity1, then add edge between them
                    elif entity1 not in G.nodes() and entity2 in G.nodes():
                        G.add_node(entity1, entity=entity1, node_type='Entity')
                        G.add_edge(entity1, entity2, relation=relation, from_article=row['title_enr_lower'])
                        G.add_edge(entity1, row['title_enr_lower'], relation="ENTITY_IN_ARTICLE")

                    # If entity1 and entity2 are not in KG, add nodes for each, then add edge between them
                    elif entity1 not in G.nodes() and entity2 not in G.nodes():
                        G.add_node(entity1, entity=entity1, node_type='Entity')
                        G.add_node(entity2, entity=entity2, node_type='Entity')
                        G.add_edge(entity1, entity2, relation=relation, from_article=row['title_enr_lower'])
                        G.add_edge(entity1, row['title_enr_lower'], relation="ENTITY_IN_ARTICLE")
                        G.add_edge(entity2, row['title_enr_lower'], relation="ENTITY_IN_ARTICLE")


# Save to Neo4j
update_database(G=G, uri=uri, user=user, pwd=pwd)


# TODO: How to load entire KG into Neo4j? Idea: Save NetworkX graph as JSON, then load JSON to Neo4j.
# Alternative: Load KG to Neo4j instance with more memory
