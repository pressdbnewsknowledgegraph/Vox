# Import Packages
## Standard Packages
from contextlib import closing

## Installed Packages
import requests

def download_http_file(url, local_filename, chunk_size=20480):
    """Downloads file from HTTP/HTTPS URL
        :param url: String representing HTTP/HTTPS URL from which to download file
        :param: local_filename: String representing name to which to save file locally
        :param: chunk_size"""
    with closing(requests.get(url, stream=True)) as r:
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=chunk_size):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
    return local_filename

url = "https://web.archive.org/web/20200809190607if_/https://download.data.world/file_download/elenadata/vox-articles/dsjVoxArticles.tsv?auth=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwcm9kLXVzZXItY2xpZW50OmNkc2Niczk4MyIsImlzcyI6ImFnZW50OmNkc2Niczk4Mzo6ZTQ4MGEwODQtMDRhZS00ZWM0LTg5YmQtZDdmOTUxMzFjNWY0IiwiaWF0IjoxNTk2OTEwODA0LCJyb2xlIjpbInVzZXIiLCJ1c2VyX2FwaV9hZG1pbiIsInVzZXJfYXBpX3JlYWQiLCJ1c2VyX2FwaV93cml0ZSJdLCJnZW5lcmFsLXB1cnBvc2UiOmZhbHNlLCJ1cmwiOiIwZTE3MTQxM2Q5M2YzNTUyODJiNDRmM2U0YjcwZDA3MzUwYTU2NTdmIn0.aoEXBJ0uQ74F4lAYlKto4NoVYf3SH-y53BDs56LFH9xDQWtkvTa14TruZly1AoN7OwdJQ28jYKWDI-DL37nYow"


download_http_file(url=url, 
local_filename="data/raw/dsjVoxArticles.tsv", chunk_size=20480)
