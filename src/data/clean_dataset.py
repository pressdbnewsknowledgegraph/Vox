# Import Packages

## Installed Packages
import pandas as pd


# Custom Functions
from data_cleaning import utf8_debugging

# Load Dataset
df = pd.read_csv("data/raw/dsjVoxArticles.tsv", sep='\t', encoding='utf-8')

# Fix records that haven't been properly tab-separated
df_bad_tab_idx = df[pd.isna(df['body'])].index.to_list()
for idx in df_bad_tab_idx:
    tab_split = df.loc[idx, "title"].split("\t")
    body = str(df.loc[idx, "author"])
    df.loc[idx, "title"] = tab_split[0]
    df.loc[idx, "author"] = tab_split[1]
    df.loc[idx, "category"] = tab_split[2]
    df.loc[idx, "published_date"] = tab_split[3]
    df.loc[idx, "updated_on"] = tab_split[4]
    df.loc[idx, "slug"] = tab_split[5]
    df.loc[idx, "blurb"] = tab_split[6]
    df.loc[idx, "body"] = body


# Fix encoding errors in titles
df['title_enr'] = df['title'].apply(func=utf8_debugging)
# df['title_enr'] = df['title'].apply(func=lambda x: x.lower().strip())
# TODO: Would making strings lower-case hamper ability of Named Entity Recognition?

# Save Interim Dataset
df.to_csv("data/interim/dsjVoxArticlesCleaned.tsv", index=False, sep='\t')
