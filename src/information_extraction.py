# Import Packages
## Installed Packages
import spacy
import networkx as nx



def get_dependency_tree(sentence, nlp):
    """Represents dependency tree of a sentence as a graph.
        Accuracy of dependency parser is dependent on specific SpaCy model.
        :param sentence: Input sentence
        :param nlp: spaCy language model
        :rtype: NetworkX Graph representing dependency parsing
        """
    # Construct dependency tree via SpaCy
    spacy_doc = nlp(sentence)

    # Add each arc in dependency tree as an edge in NetworkX Graph
    G = nx.Graph()
    for token in spacy_doc:
        for child in token.children:
            G.add_edge(token.i, child.i)
            if "text" not in G.nodes[token.i].keys():
                G.nodes[token.i]["text"] = token.text
            if "text" not in G.nodes[child.i].keys():
                G.nodes[child.i]["text"] = child.text

    # Note that a token can be a merged entity or noun chunk
    return G


def get_shortest_dependency_path(dependency_tree, entity1, entity2):
    """Calculate shortest path in dependency tree between entity 1 and entity 2.
        :param dependency_tree: Graph of dependency parsing
        :param entity1: Index of entity 1
        :param entity2: Index of entity 2
        :rtype: List of tokens representing shortest dependency path (SDP) between entity 1 and entity 2
        """
    sdp_indices = nx.shortest_path(G=dependency_tree, source=entity1, target=entity2)
    sdp_tokens = []
    for index in sdp_indices:
        sdp_tokens.append(dependency_tree.nodes[index]["text"])
    return sdp_tokens


def extract_relation(sdp):
    """Extracts a relation from a sentence, assuming the shortest dependency path between entities is the relation.
            :param sdp: Shortest dependency path between two entities
            :rtype: String representing the predicate relation between two entities
            """
    # Merge all tokens between two entities
    relation = '_'.join([x.upper() for x in sdp[1:-1]])

    # Return relation name
    return relation

